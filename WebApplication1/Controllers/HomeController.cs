﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        bilgiBilgiSayEntities db = new bilgiBilgiSayEntities();
        public ActionResult Index()
        {
            Models.HomeModel dto = new Models.HomeModel();
            dto.Konular = db.Konular.ToList().Take(4);
            return View(dto);
        }
        [ChildActionOnly]
        public ActionResult _Slider()
        {
            var liste = db.Slider.Where(x => x.BaslangicTarih < DateTime.Now && x.BitisTarih > DateTime.Now).OrderByDescending(x => x.ID).Take(4);
            return View(liste);
        }
        public ActionResult About()
        {
            ViewBag.Message = "bilgiBilgiSay hakkında...";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "İletişim Bilgileri";

            return View();
        }
        public ActionResult Gundem()
        {
            ViewBag.Message = "Gündemle sayfası.";
            return View();
        }
        public ActionResult YeniBaslik()
        {
            ViewBag.Message = "Yeni konu sayfası.";
            return View();
        }
        public ActionResult Basliklar()
        {
            var liste = db.Konular.OrderByDescending(x => x.DuzenlenmeTarihi);
            ViewBag.Message = "Konular listeli halde.";
            return View(liste);
        }
        public ActionResult ChangeCulture(string lang, string returnUrl)
        {
            Session["Culture"] = new CultureInfo(lang);
            return Redirect(returnUrl);
        }
    }
}