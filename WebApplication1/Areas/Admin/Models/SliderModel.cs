﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Areas.Admin.Models
{
    public class SliderModel
    {
        public int ID { get; set; }

        public Nullable<System.DateTime> BaslangicTarih { get; set; }

        public Nullable<System.DateTime> BitisTarih { get; set; }

        public HttpPostedFileBase Resim { get; set; }
    }
}